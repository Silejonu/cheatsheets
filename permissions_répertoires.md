# Résumé des permissions pour les répertoires sous Linux

## r

Je peux lister les noms (et rien de plus que les noms) des fichiers contenus dans le répertoire avec `ls`.

## w

Je peux modifier le contenu du répertoire.  
Les fichiers contenus dans le répertoire peuvent être la cible de `mv` ou `rm`.  
*Attention : si **x** n’est pas également appliqué, n’a aucun effet.*

## x

Je peux accéder au contenu du répertoire.  
Le répertoire peut être la cible de `cd`.  
Les fichiers contenus dans le répertoire peuvent être la cible de `ls`, `vim` ou `nano`.

# Exemples

Considérons l’arborescence suivante :

```bash
$ tree # chaque 'fichier' a toutes les permissions (rwxrwxrwx)
.
├── 0
│   └── fichier
├── r
│   └── fichier
├── rw
│   └── fichier
├── rwx
│   └── fichier
├── rx
│   └── fichier
├── w
│   └── fichier
├── wx
│   └── fichier
└── x
    └── fichier


$ ls -l
total 0
d---------. 2 root root 60 16 nov.  16:30 0
dr--r--r--. 2 root root 60 16 nov.  16:30 r
drw-rw-rw-. 2 root root 60 16 nov.  16:30 rw
drwxrwxrwx. 2 root root 60 16 nov.  16:30 rwx
dr-xr-xr-x. 2 root root 60 16 nov.  16:30 rx
d-w--w--w-. 2 root root 60 16 nov.  16:30 w
d-wx-wx-wx. 2 root root 60 16 nov.  16:30 wx
d--x--x--x. 2 root root 60 16 nov.  16:30 x
```

### Je peux utiliser `ls` sur :

```bash
rw
rwx
rx
rwx/fichier
rx/fichier
wx/fichier
x/fichier
```

### Je peux utiliser `cd` sur :

```bash
rwx
rx
wx
x
```

### Je peux utiliser `vim` ou `nano` sur :

```bash
rwx/fichier
rx/fichier
wx/fichier
x/fichier
```

### Je peux utiliser `mv` ou `rm` sur :

```bash
rwx/fichier
wx/fichier
r   # si le répertoire parent m’octroie les permissions rw ou rwx
rw  # si le répertoire parent m’octroie les permissions rw ou rwx
rwx # si le répertoire parent m’octroie les permissions rw ou rwx
rx  # si le répertoire parent m’octroie les permissions rw ou rwx
w   # si le répertoire parent m’octroie les permissions rw ou rwx
wx  # si le répertoire parent m’octroie les permissions rw ou rwx
x   # si le répertoire parent m’octroie les permissions rw ou rwx
```

## Reproduire l’arborescence de l’exemple

```bash
mkdir /tmp/repertoire_de_test
cd /tmp/repertoire_de_test
mkdir r rw rwx rx w wx x 0
for i in r rw rwx rx w wx x 0 ; do
  echo 'contenu' > ${i}/fichier
  chmod 777 ${i}/fichier
  sudo chown -R root:root ${i}
done
sudo chmod 000 ./*
sudo chmod a+r *r*
sudo chmod a+w *w*
sudo chmod a+x *x*
```