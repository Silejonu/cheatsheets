# tmux

## Commands

```bash
# Start a new session and name it
tmux new-session -s {session_name}
tmux new -s {session_name}
```

```bash
# List active sessions
tmux ls
```

```bash
# Attach to an existing session
tmux attach-session -t {session_name_or_session_ID}
tmux attach -t {session_name_or_session_ID}
tmux a -t {session_name_or_session_ID}
```

## Shortcuts

**To enter command mode, use `ctrl+b`.**

### Miscellaneous commands

`[` enter scroll mode

### Session commands

`s` list sessions  
`$` rename current session  
`d` detach current session  

### Window commands

`c` create new Window  
`,` rename current Window  
`w` list windows  
`n` next window  
`p` previous window  
`0` -> `9` move to window `n`  

### Pane commands

`%` horizontal split  
`"` vertical split  
`←`/`h` move pane left  
`→`/`i` move pane right  
`↓`/`j` move pane down  
`↑`/`k` move pane up  
`q` show pane numbers  
`q0` switch to pane number 0  
`o` loop through panes  
`{` swap current pane with previous one  
`}` swap current pane with next one  
`x` close pane  