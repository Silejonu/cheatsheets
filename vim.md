# Move around
- `w` next **w**ord (`W` to ignore symbols and punctuation)
- `e` **e**nd of current or next word (`E` to ignore symbols and punctuation)
- `b` **b**eginning of current or previous word (`B` to ignore symbols and punctuation)
- `0` jump to line start
- `^` jump to the first non-blank character on the current line
- `$` go to end of line
- `gg` **g**o to the first line in the document
- `:8` / `8gg` / `8G` **G**o to line *8*
- `G` **G**o to the last line in the document
- `%` move to the corresponding `(`, `)`, `[`, `]`, `{`, or `}` in the pair
- `(` move to previous sentence
- `)` move to next sentence
- `{` move to previous paragraph
- `}` move to next paragraph
- `f{char}` **f**ind next occurence of `{char}` in current line and jump to it (`2f{char}` for the next two)
- `F{char}` **F**ind previous occurence of `{char}` in current line and jump to it (`2F{char}` for the previous two)
- `t{char}` jump **t**o the next occurence of `{char}` in current line (`2t{char}` for the next two)
- `T{char}` jump **T**o the previous occurence of `{char}` in current line (`2T{char}` for the previous two)
- `;` repeat last `f`, `F`, `t` or `T`
- `,` repeat last `f`, `F`, `t` or `T` in reverse
- `*` move to next occurence of the word under/next the cursor
- `#` move to previous occurence of the word under/next the cursor
- `<C-f>` one page **f**orward
- `<C-b>` one page **b**ackward
- `zt` align screen with cursor at the top
- `zb` align screen with cursor at the bottom
- `zz` center screen on cursor
- `ma` **m**ark waypoint `a`
- `mA` **m**ark global waypoint `A`
- `` `a `` jump to waypoint `a`
- `'a` jump to line of waypoint `a`
## Jumps
- `<C-o>` jump to next item in jumplist
- `<C-i>` jump to previous item in jumplist
- `:jumps` print jumplist

# Edit text
- `r` **r**eplace one character
- `i` **i**nsert
- `I` **I**nsert before first non-blank character
- `a` **a**ppend/**a**dd
- `A` **A**ppend/**A**dd to end of line
- `c` **c**hange character (`c2w` / `2cw` change the next *2* words, `3cc` to change the next *3* lines) and enter insert mode
- `cc` **c**hange current line
- `C` **C**hange everything until the end of the line and enter insert mode
- `o` **o**pen a new line below the current one
- `O` **O**pen a new line above the current one
- `<<` reduce indentation (`<2<` / `2<<` reduce indentation for the next *2* lines)
- `>>` increase indentation (`>2>` / `2>>` increase indentation for the next *2* lines)
- `s` **s**ubstitute (delete character and enter insert mode, `2s` for *2* characters) 
- `S` **S**ubstitute line (delete line and enter insert mode)
- `~` invert case
- `g~w` invert case of **w**ord
- `guw` make **w**ord lowercase
- `guu` make line lowercase
- `gUw` make **w**ord uppercase
- `gUU` make line uppercase 
- `u` *(in visual mode)* transform selected text to lowercase
- `U` *(in visual mode)* transform selected text to **u**ppercase
- `J` **J**oin line to next one (remove the line-break)
- `gJ` **J**oin line to next one without adding space

# Cut/copy/paste
- `x` e**x**tract (remove)
- `X` e**X**tract (remove) previous character
- `dd` **d**elete line (`d2d` / `2dd` for the next *2* lines)
- `D` **D**elete everthing until the end of the line
- `yy` **y**ank (copy) line (`y2y` / `2yy` for the next *2* lines)
- `Y` **Y**ank (copy) until the end of line
- `p` **p**ut (**p**aste) line(s) below
- `P` **P**ut (**P**aste) line(s) above
- `""p` **p**ut last **y**anked/**d**eleted/**s**ubstituted/**c**hanged/e*x*tracted text
- `"0p` **p**ut last **y**andked line
- `"1p` **p**ut last **d**eleted line
- `"ayy` **y**ank line in register `a`
- `"Ayy` **y**ank and append line to register `a`
- `"+y` **y**ank to system clipboard
- `"*p` **p**ut from system middle mouse button clipboard
- `"_dd` **d**elete line and send it to blackhole register (do not store)
- `"acw` **c**hange **w**ord and store it into register `a`
- `:reg` print registers
- `:set paste` enter paste mode (avoid indent issues when pasting)
- `:set nopaste` leave paste mode

# Text objects
- `vi(`/`vi)` select everything **i**nside nearest `()`
- `daw` **d**elete **a**ll **w**ord
- `dis` **d**elete **i**nside (ignore outer limits) all **s**entence
- `dap` **d**elete **a**ll **p**aragraph
- `ci(`/`ci)`/`ci[`/`ci]`/`ci{`/`ci}`/`ci<`/`ci>`/`ci"`/`ci'`/`` ci` `` **c**hange **i**nside `()`/`[]`/`{}`/`<>`/`""`/`''`/``` `` ```
- `cit` **c**hange **i**nside **t**ags (*ex: change `bar` inside `<foo>bar</foo>`*)

# Cancel/revert
- `u` **u**ndo last action
- `U` **U**ndo all modifications on last modified line
- `<C-r>` **r**evert last cancelled action

# Search & replace
- `/foo` search next occurence of `foo` (during the search, `n` for the **n**ext occurence, `N` for the previous occurence)
- `?foo` search previous occurence of `foo` (during the search, `n` for the previous occurence, `N` for the **N**ext occurence)
- `/foo\c` search next occurence of `foo` and ignore **c**ase
- `:s/foo/bar` **s**ubstitute next occurence of `foo` with `bar`
- `:s/foo/bar/g` **s**ubstitue **g**lobally all occurences of `foo` with `bar` *in the current line*
- `:%s/foo/bar/g` **s**ubstitute **g**lobally all occurences of `foo` with `bar` *in the whole document*
- `:%s/foo/bar/gc` **s**ubstitute **g**lobally all occurences of `foo` with `bar` *in the whole document* and ask for **c**onfirmation
- `:90,99s/foo/bar/g` **s**ubstitute **g**lobally all occurences of `foo` with `bar` *between the lines 90 and 99*
- `:.,$,/foo/bar/g` **s**ubstitute **g**lobally all occurences of `foo` with `bar` *from the current to the last line*
- `:FOO,BAR#/bin/bash#/usr/bin/bash#g` **s**ubstitute **g**lobally all occurences of `/bin/bash` with `/usr/bin/bash` *from `FOO` to `BAR`*

# Interface
- `:set number` / `:set nu` show line **nu**mbers
- `:set nonumber` / `:nset onu` do **no**t show line **nu**mbers
- `:set relativenumber` / `:set rnu` show **r**elative line **nu**mbers
- `:set norelativenumber` / `:set nornu` do **no**t show **r**elative line **nu**mbers
- `:set list` show hidden characters
- `:set nolist` do not show hidden characters
- `:set ignorecase` / `:set ic` **i**gnore **c**ase
- `:set noignorecase` / `:set noic` do **no**t **i**gnore **c**ase
- `:set hlsearch` / `:set hls` **h**igh**l**ight **s**earch
- `:set nohlsearch` / `:set nohls` do **no**t **h**igh**l**ight **s**earch
- `:nohlsearch` / `:noh` disable hilighting until next search
- `:set incsearch` / `:set is` **i**ncremental **s**earch (search as you type)
- `:set noincsearch` / `:set nois` do **no**t use **i**ncremental **s**earch
- `<C-g>` show position in the document

# Macros
- `qa<commands>q` register macro into register `a`
- `@a` replay macro `a`
- `@@` replay last *played* macro
- `qB` record and append to macro `b`

# Save
- `:w` **w**rite (save) document
- `:w foo` **w**rite (save) document as `foo`
- `:q` **q**uit
- `:wq` **w**rite changes (save) & **q**uit
- `:q!` **q**uit without saving

# Files & buffers
- `:e[dit] /foo/bar.txt` **e**dit file `/foo/bar.txt`
- `:buffers` / `:ls` / `:files` print opened buffers
- `:b[uffer] /foo/bar.txt` open **b**uffer `/foo/bar.txt`
- `:b[uffer]3` open **b**uffer number 3
- `:bn[ext]` open **n**ext **b**uffer
- `:bp[revious]` open **p**revious **b**uffer
- `:bf[irst]` open **f**irst **b**uffer
- `:bl[ast]` open **l**ast **b**uffer
- `:b#` / `<C-^>` open previously opened buffer
- `:badd foo.txt` **add** **b**uffer `foo.txt` without opening it
- `:bd[elete] foo.txt` **d**elete **b**uffer `foo.txt` from the list
- `:bd` **d**elete (close) current **b**uffer
- `:1,3bd` **d**elete **b**uffers 1 to 3
- `:%bd` **d**elete all **b**uffers
- `:qall!` **q**uit **all** buffers without saving
- `:wall` **w**rite changes to **all** buffers
- `:bufdo set number` **set** **number** lines for all **buf**fers
- `:bufdo %s/foo/bar/g | w` **s**ubstitute `foo` for `bar` **g**lobally in all buffers then **w**rite changes
- `:Explore` **Explore** filesystem
## Buffer "tooltips"
- `a` **a**ctive
- `h` **h**idden (non-visible)
- `#` last opened
- `%` in the current window
- `+` has unsaved modifications

# Windows
- `<C-w>w` move to next **w**indow
- `<C-w>p` move to **p**revious **w**indow
- `<C-w>s` / `:sp[lit]` **s**plit current **w**indow in two horizontally (`:sp[lit] foo.txt` to open `foo.txt` in new window)
- `<C-w>v` / `:vs[plit]` split current **w**indow in two **v**ertically (`:vs[plit] foo.txt` to open `foo.txt` in new window)
- `<C-w>q` **q**uit current **w**indow
- `<C-w>o` / `:on[ly]` keep **o**nly current window, close all others
- `<C-w>+` increase **w**indow height
- `<C-w>-` decrease **w**indow height
- `<C-w><` decrease **w**indow width
- `<C-w>>` increase **w**indow width
- `crtl+w` `=` make all **w**indows same size
- `<C-w>_` make **w**indow maximum height
- `<C-w>|` make **w**indow maximum width
- `<C-w>r` **r**otate window right
- `<C-w>R` **R**otate window left
- `:ba[ll]` open **a**ll **b**uffers in their own window
- `:windo %s/foo/bar/g | w` **s**ubstitute `foo` for `bar` **g**lobally in all windows then **w**rite changes

# Folds
- `zf` create **f**old and aplly
- `zo` **o**pen fold
- `zO` **O**pen fold and its children
- `zc` **c**lose fold
- `zC` **C**lose fold and its children
- `za` toggle fold
- `zA` toogle fold recursively
- `zm` fold **m**ore
- `zM` fold **M**ore (close all folds)
- `zr` **r**educe folding
- `zR` **R**educe folding (open all folds)
- `:set foldmethod=indent` / `:set foldmethod=syntax` fold based on indent/syntax instead of manually

# Miscellaneous
- `.` repeat last action
- `<C-n>` / `<C-p>` cycle through auto-completion
- `<C-v>I#<Esc>` **i**nsert `#` at the beginning of all selected lines
- `<C-v>$Afoo<Esc>` **a**ppend `foo` the end of all selected lines
- `gcc` (un-)comment current line (`3gcc` for 3 lines starting from current) *[Neovim 0.10+ or `vim-commentary`]*
- `gcma` (un-)comment from current line until **m**ark `a` *[Neovim 0.10+ or `vim-commentary`]*
- `:put=range(1,5)` generate lines with number 1 to 5
- `:put=range(10,0,-1)` generate lines with number 10 to 0
- `<C-a>` increment all selected numbers by 1
- `<C-x>` decrement all selected numbers by 1
- `g<C-a>` increment all selected numbers by 1 + 1 per line
- `10g<C-a>` increment all selected numbers by 10 + 10 per line
- `:center` **center**-align (`:center 40` to change max width from default 80)
- `:left` **left**-align (`:left 10` to leave 10 spaces before)
- `:right` **right**-align (`:right 70` to change rightmost position from default 80)

# Modes
## Normal mode
- `<Esc>`
## Insert mode
- `i`
- `I` enter **i**nsert mode at the beginning of the *text* on the current line
## Visual mode
- `v` character-wise **v**isual mode
- `V` line-wise **V**isual mode
- `<C-v>` block-wise **v**isual mode
- `o` jump to **o**ther end of selection (`O` jumps horizontally)
## Replace mode
- `R` **R**eplace until cancelled (`backspace` to undo edits)
## Command mode
- `:!foo` run external command `foo`
- `:r foo` **r**ead (and insert) content of file `foo`
- `:r !foo` **r**ead (and insert) output of command `foo`
- `:help` / `:h` open **h**elp menu
- `:help foo` / `:h foo` get **h**elp on `foo`
- `:%d` **d**elete whole document

# Plugins
## nvim-tree.lua
- `<leader>e` toggle **e**xplorer (file tree)
- `a foo` create file `foo`
- `a bar/` create directory `bar`
- `H` toggle show **H**idden files
- `I` toggle show git **I**gnored files
- `r` **r**ename file
- `d` **d**elete file
- `x` e**x**tract file (prepare it to be moved)
- `c` **c**opy file
- `p` **p**ut file
- `E` **E**xpand all directories
- `W` collapse all directories
## markdown-preview.nvim
`:MarkdownPreview` start the preview
`:MarkdownPreviewStop` stop the preview
## toggleterm.nvim
- `<leader>t` open **t**erminal
- `<Esc><Esc>` exit terminal
- `<C-\><C-n>` enter **n**ormal mode when in terminal mode
