# Bash shell shortcuts

```bash
!$                   # last argument from previous command
!^                   # first argument from previous command
!*                   # all arguments from previous command
!:2                  # second argument from previous command
!:2-4                # second to fourth argument from previous command
!!                   # previous command
!5                   # 5th command in history
!foo                 # most recent command starting with foo
!?foo?               # most recent comment containing foo
!!:s/find/replace    # last command, substitute find with replace
!6:p                 # append 6th command to end of history, without running it
```

`ctrl+C` kill current command  
`ctrl+L` clear the terminal  
`ctrl+D` closes the current terminal  
`ctrl+R` search through history  
`ctrl+A` move to the start of the command line  
`ctrl+E` move to the end of the command line  
`ctrl+U` delete everything from the cursor to the start of the line  
`ctrl+K` delete everything from the cursor to the end of the line  
`ctrl+S` stop command output to the screen  
`ctrl+Q` resume command output to the screen  
`ctrl+Z` stop command (`fg` or `bg` to resume)